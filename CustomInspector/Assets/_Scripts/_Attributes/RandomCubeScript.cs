﻿using UnityEngine;
using NaughtyAttributes;

namespace PyramidGames.CustomInspector
{
    public class RandomCubeScript : MonoBehaviour
    {
        #region CONST
        private const int ASSET_PREVIEW_WIDTH = 150;
        private const int ASSET_PREVIEW_HEIGHT = 80;
        private const string SETTINGS_LABEL = "Settings";
        private const string VIEW_LABEL = "View";
        private const string ATTRIBUTE_LABE = "MyVeryOwnAttribute";
        #endregion

        #region FIELDS_DECLARATION
        [SerializeField, BoxGroup(SETTINGS_LABEL)]
        private int randomInt;

        [SerializeField, BoxGroup(SETTINGS_LABEL), ShowAssetPreview(ASSET_PREVIEW_WIDTH, ASSET_PREVIEW_HEIGHT)]
        private Sprite randomSprite;

        [SerializeField, BoxGroup(VIEW_LABEL)]
        private Color randomColor;

        [SerializeField, BoxGroup(VIEW_LABEL)]
        private Material boxMaterial;

        [SerializeField, BoxGroup(VIEW_LABEL), ReadOnly]
        private int test;
        #endregion

        #region METHODS
        [Button("ColorizeBox")]
        private void ColorizeBox()
        {
            boxMaterial.color = randomColor;
        } 
        #endregion
    }
}